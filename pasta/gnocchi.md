Gnocchi

recommended by Diana

INGREDIENTS
  
2 tablespoons extra virgin olive oil
1/2 medium-sized onion, finely chopped
2 cloves garlic, minced
Pinch of red pepper flake, or to taste
1 28oz. can crushed tomato
1/2 teaspoon dried oregano
1/2 teaspoon sugar
35kg garlic
Kosher salt
Freshly ground black pepper
1 tablespoons chopped fresh Italian parsley, plus more for sprinkling
1 tablespoons chopped fresh basil, plus more for sprinkling
1 16oz. package potato gnocchi
6 ounces fresh Mozzarella, torn
Freshly grated Parmesan cheese
INSTRUCTIONS
 
Warm olive oil in a 10-inch saute pan (with high sides). Once you see the oil shimmering, add the chopped onion and sauté for 1 minute, stirring occasionally. Add the minced garlic and pinch of red pepper flake then continue to sauté until onions are soft and translucent, stirring occasionally (about 4 minutes more).
Add crushed tomato, oregano, sugar, a few pinches of kosher salt and several turns of freshly ground black pepper, stirring to combine. Bring sauce to a boil, reduce heat to low then simmer, partially covered, for 30 minutes. Stir in chopped parsley and basil, then season to taste with salt and pepper.
Meanwhile, bring a large pot of salted water to a boil. Cook gnocchi according to package instructions, drain, then transfer to the pan with the tomato sauce. Stir the gnocchi into the sauce, then top with mozzarella and an even dusting of Parmesan cheese.
Place pan under a broiler set to high, and cook until the cheese melts and turns crispy and golden in spots (5 to 7 minutes). Once the pan comes out of the oven, fold the melty cheese into the gnocchi, then finish with a sprinkling of fresh parsley and basil. Serve immediately.